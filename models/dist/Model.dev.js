"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CategoryModel = exports.ProductModel = exports.UserModel = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Schema = _mongoose["default"].Schema;
var UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    require: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    min: 6,
    max: 20
  }
});

var UserModel = _mongoose["default"].model("User", UserSchema);

exports.UserModel = UserModel;
var ProductSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  rate: {
    type: Number,
    required: true,
    min: 1
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  }
});

var ProductModel = _mongoose["default"].model("Product", ProductSchema);

exports.ProductModel = ProductModel;
var CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  }
});

var CategoryModel = _mongoose["default"].model("Category", CategorySchema);

exports.CategoryModel = CategoryModel;