import mongoose from "mongoose";
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, require: true, unique: true },
  password: { type: String, required: true, min: 6, max:20},
});

const UserModel = mongoose.model("User", UserSchema);

const ProductSchema = new Schema({
  title: { type: String, required: true },
  image: {type: String,required:true},
  rate: { type: Number, required: true, min: 1 },
  category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
})

const ProductModel = mongoose.model("Product", ProductSchema);

const CategorySchema = new Schema({
  name: { type: String, required: true },
})

const CategoryModel = mongoose.model("Category", CategorySchema);

export {UserModel,ProductModel,CategoryModel};
