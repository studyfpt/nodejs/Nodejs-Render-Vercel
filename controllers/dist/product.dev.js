"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Model = require("../models/Model.js");

var _product = require("../validate/product.js");

var ProductController = {
  getAll: function getAll(req, res) {
    var products;
    return regeneratorRuntime.async(function getAll$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return regeneratorRuntime.awrap(_Model.ProductModel.find().maxTimeMS(30000));

          case 3:
            products = _context.sent;
            res.status(200).json({
              message: "Get All Movie Done",
              data: products
            });
            _context.next = 10;
            break;

          case 7:
            _context.prev = 7;
            _context.t0 = _context["catch"](0);
            res.status(400).json({
              message: "Get All Movie Error" + _context.t0
            });

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  getCategory: function getCategory(req, res) {
    var category;
    return regeneratorRuntime.async(function getCategory$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return regeneratorRuntime.awrap(_Model.ProductModel.findById(req.params.id).populate({
              path: 'category',
              select: 'name'
            }));

          case 3:
            category = _context2.sent;
            res.status(200).json({
              message: "Get Category Movie Done",
              data: category
            });
            _context2.next = 10;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](0);
            res.status(400).json({
              message: "Get Category Movie Error" + _context2.t0
            });

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  getDetail: function getDetail(req, res) {
    var product;
    return regeneratorRuntime.async(function getDetail$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return regeneratorRuntime.awrap(_Model.ProductModel.findById(req.params.id).populate({
              path: 'category',
              select: 'name'
            }));

          case 3:
            product = _context3.sent;
            res.status(200).json({
              message: "Get Detail Movie Done",
              data: product
            });
            _context3.next = 10;
            break;

          case 7:
            _context3.prev = 7;
            _context3.t0 = _context3["catch"](0);
            res.status(400).json({
              message: "Get Detail Movie Error" + _context3.t0
            });

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  create: function create(req, res) {
    var _req$body, title, image, rate, category, _createProductSchema$, error, existingCategory, product;

    return regeneratorRuntime.async(function create$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            console.log(req.body);
            _req$body = req.body, title = _req$body.title, image = _req$body.image, rate = _req$body.rate, category = _req$body.category;
            _createProductSchema$ = _product.createProductSchema.validate({
              title: title,
              image: image,
              rate: rate,
              category: category
            }, {
              abortEarly: false
            }), error = _createProductSchema$.error;

            if (!error) {
              _context4.next = 6;
              break;
            }

            return _context4.abrupt("return", res.status(404).json({
              message: error.details.map(function (value) {
                return value.message;
              })
            }));

          case 6:
            _context4.next = 8;
            return regeneratorRuntime.awrap(_Model.CategoryModel.findById(category));

          case 8:
            existingCategory = _context4.sent;

            if (existingCategory) {
              _context4.next = 11;
              break;
            }

            return _context4.abrupt("return", res.status(400).send("ID loại phim không tồn tại"));

          case 11:
            product = new _Model.ProductModel({
              title: title,
              image: image,
              rate: rate,
              category: category
            });
            _context4.next = 14;
            return regeneratorRuntime.awrap(product.save());

          case 14:
            res.status(201).json({
              message: "Thêm phim thành công",
              data: req.body
            });
            _context4.next = 20;
            break;

          case 17:
            _context4.prev = 17;
            _context4.t0 = _context4["catch"](0);
            res.status(500).json({
              message: "Thêm phim thất bại - " + _context4.t0.message
            });

          case 20:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[0, 17]]);
  },
  "delete": function _delete(req, res) {
    var existingProduct;
    return regeneratorRuntime.async(function _delete$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _context5.next = 3;
            return regeneratorRuntime.awrap(_Model.ProductModel.findById(req.params.id));

          case 3:
            existingProduct = _context5.sent;

            if (existingProduct) {
              _context5.next = 6;
              break;
            }

            return _context5.abrupt("return", res.status(404).json({
              message: "Phim không tồn tại"
            }));

          case 6:
            _context5.next = 8;
            return regeneratorRuntime.awrap(_Model.ProductModel.findByIdAndDelete(req.params.id));

          case 8:
            res.status(200).json({
              message: "Delete Product Name ".concat(existingProduct.title, " Ok")
            });
            _context5.next = 14;
            break;

          case 11:
            _context5.prev = 11;
            _context5.t0 = _context5["catch"](0);
            res.status(500).json({
              message: "Xóa phim thất bại - " + _context5.t0.message
            });

          case 14:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[0, 11]]);
  },
  update: function update(req, res) {
    var productId, existingProduct, _req$body2, title, image, rate, category, _updateProductSchema$, error, existingCategory, product;

    return regeneratorRuntime.async(function update$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            productId = req.params.id;
            _context6.next = 4;
            return regeneratorRuntime.awrap(_Model.ProductModel.findById(productId));

          case 4:
            existingProduct = _context6.sent;

            if (existingProduct) {
              _context6.next = 7;
              break;
            }

            return _context6.abrupt("return", res.status(404).json({
              message: "phim không tồn tại"
            }));

          case 7:
            _req$body2 = req.body, title = _req$body2.title, image = _req$body2.image, rate = _req$body2.rate, category = _req$body2.category;
            _updateProductSchema$ = _product.updateProductSchema.validate({
              title: title,
              image: image,
              rate: rate,
              category: category
            }, {
              abortEarly: false
            }), error = _updateProductSchema$.error;

            if (error) {
              res.status(404).json({
                message: error.details.map(function (value) {
                  return value.message;
                })
              });
            }

            _context6.next = 12;
            return regeneratorRuntime.awrap(_Model.CategoryModel.findById(category));

          case 12:
            existingCategory = _context6.sent;

            if (existingCategory) {
              _context6.next = 15;
              break;
            }

            return _context6.abrupt("return", res.status(400).send("ID loại phim không tồn tại"));

          case 15:
            _context6.next = 17;
            return regeneratorRuntime.awrap(_Model.ProductModel.findById(productId));

          case 17:
            product = _context6.sent;
            existingProduct.title = title;
            existingProduct.image = image;
            existingProduct.rate = rate;
            existingProduct.category = category;
            _context6.next = 24;
            return regeneratorRuntime.awrap(existingProduct.save());

          case 24:
            res.status(201).json({
              message: "Sửa phim thành công",
              data_old: product,
              data_new: existingProduct
            });
            _context6.next = 30;
            break;

          case 27:
            _context6.prev = 27;
            _context6.t0 = _context6["catch"](0);
            res.status(500).json({
              message: "Cập nhật phim thất bại - " + _context6.t0.message
            });

          case 30:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[0, 27]]);
  }
};
var _default = ProductController;
exports["default"] = _default;