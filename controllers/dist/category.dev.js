"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Model = require("../models/Model.js");

var CategoryController = {
  Create: function Create(req, res) {
    var name, existedName, category;
    return regeneratorRuntime.async(function Create$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            name = req.body.name;
            _context.next = 4;
            return regeneratorRuntime.awrap(_Model.CategoryModel.findOne({
              name: name
            }));

          case 4:
            existedName = _context.sent;

            if (!existedName) {
              _context.next = 7;
              break;
            }

            return _context.abrupt("return", res.status(404).json({
              message: "Tên loại sản phẩm đã tồn tại !"
            }));

          case 7:
            _context.next = 9;
            return regeneratorRuntime.awrap(_Model.CategoryModel.create({
              name: name
            }));

          case 9:
            category = _context.sent;
            return _context.abrupt("return", res.status(201).json({
              message: "Tạo loại sản phẩm thành công !",
              category: category
            }));

          case 13:
            _context.prev = 13;
            _context.t0 = _context["catch"](0);
            res.status(500).json({
              message: "Error" + _context.t0
            });

          case 16:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[0, 13]]);
  },
  All: function All(req, res) {
    var categorys;
    return regeneratorRuntime.async(function All$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return regeneratorRuntime.awrap(_Model.CategoryModel.find());

          case 3:
            categorys = _context2.sent;
            res.status(200).json({
              message: "Lấy tất cả loại sản phẩm thành công !",
              data: categorys
            });
            _context2.next = 10;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](0);
            res.status(500).json({
              message: "Error: " + _context2.t0.message
            });

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[0, 7]]);
  }
};
var _default = CategoryController;
exports["default"] = _default;