import bcryptjs from "bcryptjs";
import { UserModel } from "../models/Model.js";
import { loginShema, registerShema } from "../validate/auth.js";
import jwt  from "jsonwebtoken";

const UserController = {
  Register: async (req, res) => {
    try {
      const { name, email, password } = req.body;
      const { error } = registerShema.validate(
        { name, email, password },
        { abortEarly: false }
      );
      if (error) {
        return res.status(400).json({
          message: error.details.map((value) => {
            return value.message;
          }),
        });
      }
      const existedEmail = await UserModel.findOne({ email: email });
      console.log(existedEmail);
      if (existedEmail) {
        return res.status(400).json({
          message: "Email đã được đăng ký !",
        });
      }
      const hashPassword = await bcryptjs.hash(password, 10);
      const user = await UserModel.create({
        name,
        email,
        password: hashPassword,
      });
      return res.status(201).json({
        message: "Đăng Ký Thành Công !",
        user: { ...user.toObject(), password: undefined },
      });
    } catch (error) {
      return res.status(400).json({
        message: "Đăng Ký Thất Bại !",
        error,
      });
    }
  },
  Login: async (req, res) => {
    try {
      const { email, password } = req.body;
      const { error } = loginShema.validate(
        { email, password },
        { abortEarly: false }
      );
        if (error) {
        return res.status(400).json({
          message: error.details.map((value) => {
            return value.message;
          }),
        });
      }
      const user = await UserModel.findOne({ email });
      if (!user) {
        return res.status(404).json({
          message: "Người dùng chưa đăng ký tài khoản !",
        });
      }
      const checkPassword = await bcryptjs.compare(password, user.password);
      if (!checkPassword) {
        return res.status(401).json({
          message: "Mật khẩu không đúng !",
        });
      }
      const token = jwt.sign({ user: user._id }, process.env.KEY, { expiresIn: '1w' });
      return res.status(200).json({
        message: "Đăng Nhập Thành Công !",
        user: { ...user.toObject(), password: undefined },
        token
      });
    } catch (error) {
      return res.status(400).json({
        message: "Đăng Nhập Thất Bại !",
        error,
      });
    }
  },
};

export default UserController;
