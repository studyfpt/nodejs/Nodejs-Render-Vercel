import { CategoryModel, ProductModel } from "../models/Model.js";
import { createProductSchema, updateProductSchema } from "../validate/product.js";
const ProductController = {
  getAll: async (req, res) => {
    try {
      const products = await ProductModel.find().maxTimeMS(30000);
      res.status(200).json({
        message: "Get All Movie Done",
        data:products,
      });
    } catch (error) {
      res.status(400).json({
        message: "Get All Movie Error" + error,
      })
    }
  
  
  },
  getCategory: async (req, res) => {
    try {
      const category = await ProductModel.findById(req.params.id).populate({
        path: 'category',
        select: 'name'
      });
      res.status(200).json({
        message: "Get Category Movie Done",
        data:category,
      });
    } catch (error) {
      res.status(400).json({
        message: "Get Category Movie Error" + error,
      })
    }
  
  
  },
  getDetail: async (req, res) => {
    try {
      const product = await ProductModel.findById(req.params.id).populate({
        path: 'category',
        select: 'name'
      });
      res.status(200).json({
        message: "Get Detail Movie Done",
        data:product,
      });
    } catch (error) {
      res.status(400).json({
        message: "Get Detail Movie Error" + error,
      })
    }
  },
  create: async (req, res) => {
    try {
      console.log(req.body);
      const { title, image, rate, category } = req.body;
       const { error } = createProductSchema.validate(
        { title,image, rate, category},
        { abortEarly: false }
      );
      if (error) {
        return res.status(404).json({
          message: error.details.map((value) => {
            return value.message;
          }),
        });
      }
      const existingCategory = await CategoryModel.findById(category);
      if (!existingCategory) {
        return res.status(400).send("ID loại phim không tồn tại");
      }
      const product = new ProductModel({
        title,
        image,
        rate,
        category
      });
      await product.save();
      res.status(201).json({
        message: "Thêm phim thành công",
        data:req.body
      });
    } catch (error) {
      res.status(500).json({
        message:"Thêm phim thất bại - " + error.message
      });
    }
  },
  delete: async (req, res) => {
    try {
      const existingProduct = await ProductModel.findById(req.params.id);
      if (!existingProduct) {
        return res.status(404).json({
          message : "Phim không tồn tại"
        });
      }
      await ProductModel.findByIdAndDelete(req.params.id);
      res.status(200).json({
        message : `Delete Product Name ${existingProduct.title} Ok`
      });
    } catch (error) {
      res.status(500).json({
        message : "Xóa phim thất bại - " + error.message
      });
    }
  },
  update: async (req, res) => {
    try {
      const productId = req.params.id;
      const existingProduct = await ProductModel.findById(productId);
      if (!existingProduct) {
        return res.status(404).json({
          message : "phim không tồn tại"
        });
      }
      const { title, image, rate, category } = req.body;
       const { error } = updateProductSchema.validate(
        { title,image, rate, category},
        { abortEarly: false }
      );
      if (error) {
        res.status(404).json({
          message: error.details.map((value) => {
            return value.message;
          }),
        });
      }
      const existingCategory = await CategoryModel.findById(category);
      if (!existingCategory) {
        return res.status(400).send("ID loại phim không tồn tại");
      }
      const product = await ProductModel.findById(productId);
      existingProduct.title = title;
      existingProduct.image = image;
      existingProduct.rate = rate;
      existingProduct.category = category;
      await existingProduct.save();

      res.status(201).json({
        message: "Sửa phim thành công",
        data_old: product,
        data_new:existingProduct,
      });
    } catch (error) {
      res.status(500).json({
        message : "Cập nhật phim thất bại - " + error.message
      });
    }
  },
};

export default ProductController;
