import { CategoryModel } from "../models/Model.js";

const CategoryController = {
  Create: async (req, res) => {
    try {
      const { name } = req.body;
      const existedName = await CategoryModel.findOne({ name: name });
      if (existedName) {
        return res.status(404).json({
          message: "Tên loại sản phẩm đã tồn tại !",
        });
      }
      const category = await CategoryModel.create({
        name
      });
      return res.status(201).json({
        message: "Tạo loại sản phẩm thành công !",
        category,
      });
    } catch (error) {
      res.status(500).json({
        message: "Error" + error,
      });
    }
  },
  All: async (req, res) => {
    try {
      const categorys = await CategoryModel.find();
      res.status(200).json({
        message: "Lấy tất cả loại sản phẩm thành công !",
        data: categorys,
      });
    } catch (error) {
      res.status(500).json({
        message: "Error: " + error.message,
      });
    }
  },
}

export default CategoryController;
