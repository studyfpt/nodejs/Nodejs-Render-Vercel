import express from "express";
import routes from './routes/index.js';
import mongoose from 'mongoose';
import bodyParser from "body-parser";
import dotenv from 'dotenv';
import cors from 'cors';  // Importing cors
import ProductController from "./controllers/product.js";
dotenv.config();

async function createServer() {
  const app = express();
  const port = process.env.PORT || 3000;
  try {
    await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      serverSelectionTimeoutMS: 30000,
    });
    console.log("Kết nối thành công đến MongoDB!");
  } catch (error) {
    console.error("Kết nối thất bại đến MongoDB:", error);
  }
app.use(cors({
  origin: 'http://localhost:3000', // Chỉ cho phép localhost:3000
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  credentials: true
}));

  app.set("view engine", "ejs");
  app.set("views", "./views");
  app.use(express.static("public"));
  app.use(bodyParser.json()); 
  app.use(bodyParser.urlencoded({ extended: true }));

  routes(app);

  app.get('/', ProductController.getAll);

  app.listen(port, () => {
    console.log(`Ứng dụng ví dụ đang lắng nghe trên cổng ${port}`);
  });
}

createServer();

export default createServer;
