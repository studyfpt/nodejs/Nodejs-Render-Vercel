"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _index = _interopRequireDefault(require("./routes/index.js"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _product = _interopRequireDefault(require("./controllers/product.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_dotenv["default"].config();

function createServer() {
  var app, port;
  return regeneratorRuntime.async(function createServer$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          app = (0, _express["default"])();
          port = process.env.PORT || 3000;
          _context.prev = 2;
          _context.next = 5;
          return regeneratorRuntime.awrap(_mongoose["default"].connect(process.env.DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            serverSelectionTimeoutMS: 30000 // Timeout trong mili giây (30 giây)

          }));

        case 5:
          console.log("Kết nối thành công đến MongoDB!");
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](2);
          console.error("Kết nối thất bại đến MongoDB:", _context.t0);

        case 11:
          app.set("view engine", "ejs");
          app.set("views", "./views");
          app.use(_express["default"]["static"]("public"));
          app.use(_bodyParser["default"].json());
          app.use(_bodyParser["default"].urlencoded({
            extended: true
          }));
          (0, _index["default"])(app);
          app.get('/', _product["default"].getAll);
          app.listen(port, function () {
            console.log("\u1EE8ng d\u1EE5ng v\xED d\u1EE5 \u0111ang l\u1EAFng nghe tr\xEAn c\u1ED5ng ".concat(port));
          });

        case 19:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[2, 8]]);
}

createServer();
var _default = createServer;
exports["default"] = _default;