import categoryRouter from "./category.js";
import productRouter from "./product.js";
import userRouter from "./user.js";

function routes(app) {
  app.get('/', (req, res) => {
    res.send("Server is working");
  })
  app.use("/user", userRouter);
  app.use("/product", productRouter);
  app.use("/category", categoryRouter);
}
export default routes;