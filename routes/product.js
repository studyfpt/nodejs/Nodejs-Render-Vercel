import express from "express";
import { checkPermission } from "../middlewares/authenticate.js";
const productRouter = express.Router();

import ProductControler from "../controllers/product.js";
  productRouter.get("/", ProductControler.getAll);
  productRouter.get("/category/:id", ProductControler.getCategory);
  productRouter.post("/",checkPermission, ProductControler.create);
  productRouter.get("/:id", ProductControler.getDetail);
  productRouter.put("/:id",checkPermission, ProductControler.update);
  productRouter.delete("/:id",checkPermission, ProductControler.delete);
export default productRouter;