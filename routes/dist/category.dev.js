"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _category = _interopRequireDefault(require("../controllers/category.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var categoryRouter = _express["default"].Router();

categoryRouter.get("/", _category["default"].All);
categoryRouter.post("/", _category["default"].Create);
var _default = categoryRouter;
exports["default"] = _default;