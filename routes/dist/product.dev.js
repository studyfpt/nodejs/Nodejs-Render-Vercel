"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _authenticate = require("../middlewares/authenticate.js");

var _product = _interopRequireDefault(require("../controllers/product.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var productRouter = _express["default"].Router();

productRouter.get("/", _product["default"].getAll);
productRouter.get("/category/:id", _product["default"].getCategory);
productRouter.post("/", _authenticate.checkPermission, _product["default"].create);
productRouter.get("/:id", _product["default"].getDetail);
productRouter.put("/:id", _authenticate.checkPermission, _product["default"].update);
productRouter["delete"]("/:id", _authenticate.checkPermission, _product["default"]["delete"]);
var _default = productRouter;
exports["default"] = _default;