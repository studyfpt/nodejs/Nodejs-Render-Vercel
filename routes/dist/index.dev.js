"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _category = _interopRequireDefault(require("./category.js"));

var _product = _interopRequireDefault(require("./product.js"));

var _user = _interopRequireDefault(require("./user.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function routes(app) {
  app.get('/', function (req, res) {
    res.send("Server is working");
  });
  app.use("/user", _user["default"]);
  app.use("/product", _product["default"]);
  app.use("/category", _category["default"]);
}

var _default = routes;
exports["default"] = _default;