import express from 'express';
import CategoryController from '../controllers/category.js';

const categoryRouter = express.Router();
categoryRouter.get("/",CategoryController.All);
categoryRouter.post("/",CategoryController.Create);
export default categoryRouter;