import express from "express";
import UserController from "../controllers/user.js";

const userRouter = express.Router();

userRouter.post("/login", UserController.Login);
userRouter.post("/register", UserController.Register);

export default userRouter;
