"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginShema = exports.registerShema = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var checkEmail = function checkEmail(email, helpers) {
  if (!/\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b/.test(email)) {
    return helpers.message({
      custom: 'Trường Email phải là email hợp lệ'
    });
  }

  var domain = email.split('@')[1];

  if (domain.length > 255) {
    return helpers.message({
      custom: 'Tên miền của email quá dài'
    });
  }

  return email;
};

var registerShema = _joi["default"].object({
  name: _joi["default"].string().required().messages({
    "any.required": "Trường Name là bắt buộc",
    "string.empty": "Trường Name không được để trống"
  }),
  email: _joi["default"].string().email().custom(checkEmail, 'Validate email !').required().messages({
    "any.required": "Trường Email là bắt buộc",
    "string.empty": "Trường Email không được để trống",
    "string.email": "Trường Email phải là email hợp lệ"
  }),
  password: _joi["default"].string().min(6).max(20).required().messages({
    "any.required": "Trường Password là bắt buộc",
    "string.empty": "Trường Password không được để trống",
    "string.min": "Trường Password phải có ít nhất {#limit} ký tự",
    "string.max": "Trường Password không được vượt quá {#limit} ký tự"
  })
});

exports.registerShema = registerShema;

var loginShema = _joi["default"].object({
  email: _joi["default"].string().email().custom(checkEmail, 'Validate email !').required().messages({
    "any.required": "Trường email là bắt buộc !",
    "string.empty": "Trường email không được để trống",
    "string.email": "Trường email phải hợp lệ !"
  }),
  password: _joi["default"].string().min(6).max(20).required().messages({
    "any.required": "Trường password là bắt buộc !",
    "string.empty": "Trường password không được để trống !",
    "string.min": "Trường password phải có hơn {#limit} ký tự !",
    "string.max": "Trường password không được vượt quá {#limit} ký tự !"
  })
});

exports.loginShema = loginShema;