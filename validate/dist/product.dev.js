"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateProductSchema = exports.createProductSchema = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var createProductSchema = _joi["default"].object({
  title: _joi["default"].string().required().min(6).max(200).messages({
    "any.required": "Trường Title là bắt buộc",
    "string.empty": "Trường Title không được để trống",
    "string.min": "Trường Title phải có ít nhất {#limit} ký tự",
    "string.max": "Trường Title không được vượt quá {#limit} ký tự"
  }),
  image: _joi["default"].string().required().messages({
    "any.required": "Trường Image là bắt buộc",
    "string.empty": "Trường Image không được để trống"
  }),
  rate: _joi["default"].number().required().min(1).max(5).messages({
    "any.required": "Trường Rate là bắt buộc",
    "number.base": "Trường Rate phải là một số",
    "number.min": "Trường Rate phải lớn hơn hoặc bằng {#limit}",
    "number.max": "Trường Rate phải nhỏ hơn hoặc bằng {#limit}"
  }),
  category: _joi["default"].string().required().messages({
    "any.required": "Trường category là bắt buộc",
    "string.empty": "Trường category không được để trống"
  })
});

exports.createProductSchema = createProductSchema;

var updateProductSchema = _joi["default"].object({
  title: _joi["default"].string().min(6).max(200).messages({
    "string.min": "Trường Title phải có ít nhất {#limit} ký tự",
    "string.max": "Trường Title không được vượt quá {#limit} ký tự"
  }),
  image: _joi["default"].string().messages({
    "string.empty": "Trường Image không được để trống"
  }),
  rate: _joi["default"].number().min(1).max(5).messages({
    "number.base": "Trường Rate phải là một số",
    "number.min": "Trường Rate phải lớn hơn hoặc bằng {#limit}",
    "number.max": "Trường Rate phải nhỏ hơn hoặc bằng {#limit}"
  }),
  category: _joi["default"].string().messages({
    "string.empty": "Trường category không được để trống"
  })
});

exports.updateProductSchema = updateProductSchema;