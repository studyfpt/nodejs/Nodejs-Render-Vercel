import Joi from "joi";

const createProductSchema = Joi.object({
    title: Joi.string().required().min(6).max(200).messages({
        "any.required": "Trường Title là bắt buộc",
        "string.empty": "Trường Title không được để trống",
        "string.min": "Trường Title phải có ít nhất {#limit} ký tự",
        "string.max": "Trường Title không được vượt quá {#limit} ký tự"
    }),
    image: Joi.string().required().messages({
        "any.required": "Trường Image là bắt buộc",
        "string.empty": "Trường Image không được để trống"
    }),
    rate: Joi.number().required().min(1).max(5).messages({
        "any.required": "Trường Rate là bắt buộc",
        "number.base": "Trường Rate phải là một số",
        "number.min": "Trường Rate phải lớn hơn hoặc bằng {#limit}",
        "number.max": "Trường Rate phải nhỏ hơn hoặc bằng {#limit}"
    }),
    category: Joi.string().required().messages({
        "any.required": "Trường category là bắt buộc",
        "string.empty": "Trường category không được để trống",
    }),
});

const updateProductSchema = Joi.object({
    title: Joi.string().min(6).max(200).messages({
        "string.min": "Trường Title phải có ít nhất {#limit} ký tự",
        "string.max": "Trường Title không được vượt quá {#limit} ký tự"
    }),
    image: Joi.string().messages({
        "string.empty": "Trường Image không được để trống"
    }),
    rate: Joi.number().min(1).max(5).messages({
        "number.base": "Trường Rate phải là một số",
        "number.min": "Trường Rate phải lớn hơn hoặc bằng {#limit}",
        "number.max": "Trường Rate phải nhỏ hơn hoặc bằng {#limit}"
    }),
    category: Joi.string().messages({
        "string.empty": "Trường category không được để trống",
    }),
});

export { createProductSchema, updateProductSchema };

