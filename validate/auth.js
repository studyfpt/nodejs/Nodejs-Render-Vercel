import Joi from "joi";
const checkEmail = (email, helpers) => {
  if (!/\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b/.test(email)) {
    return helpers.message({ custom: 'Trường Email phải là email hợp lệ' });
  }
  const domain = email.split('@')[1];
  if (domain.length > 255) {
    return helpers.message({ custom: 'Tên miền của email quá dài' });
  }
  return email;
};
const registerShema = Joi.object({
  name: Joi.string().required().messages({
    "any.required": "Trường Name là bắt buộc",
    "string.empty": "Trường Name không được để trống",
  }),
  email: Joi.string().email().custom(checkEmail, 'Validate email !').required().messages({
    "any.required": "Trường Email là bắt buộc",
    "string.empty": "Trường Email không được để trống",
    "string.email": "Trường Email phải là email hợp lệ",
  }),
  password: Joi.string().min(6).max(20).required().messages({
    "any.required": "Trường Password là bắt buộc",
    "string.empty": "Trường Password không được để trống",
    "string.min": "Trường Password phải có ít nhất {#limit} ký tự",
    "string.max": "Trường Password không được vượt quá {#limit} ký tự",
  }),
});
const loginShema = Joi.object({
  email: Joi.string().email().custom(checkEmail, 'Validate email !').required().messages({
    "any.required": "Trường email là bắt buộc !",
    "string.empty": "Trường email không được để trống",
    "string.email": "Trường email phải hợp lệ !",
  }),
  password: Joi.string().min(6).max(20).required().messages({
    "any.required": "Trường password là bắt buộc !",
    "string.empty": "Trường password không được để trống !",
    "string.min": "Trường password phải có hơn {#limit} ký tự !",
    "string.max": "Trường password không được vượt quá {#limit} ký tự !",
  }),
});
export { registerShema, loginShema };
