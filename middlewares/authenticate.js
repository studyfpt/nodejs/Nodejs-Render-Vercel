import jwt from 'jsonwebtoken';
import { UserModel } from "../models/Model.js";
const checkPermission = async (req, res, next) => {
    try {
        const token = req.headers.authorization?.split(' ')[1];
        if (!token) {
            return res.status(401).json({
                message : "Không có token !"
            })
        }
        const data = jwt.verify(token, process.env.KEY)
        if (!data) {
            return res.status(401).json({
                message : "Không có tài khoản !"
            })
        }

        //check user
        const user = await UserModel.findById(data.user)
        if (!user) {
            return res.status(404).json({
                message : "Không tìm thấy tài khoản !"
            })
        }
        console.log(user);
        next();
    } catch (error) {
        res.status(401).json({
            message : error
        })
    }
}
export {checkPermission}